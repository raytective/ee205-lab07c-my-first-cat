///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello1.cpp
/// @version 1.0
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date 28_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
using namespace std;

int main() {


   cout << "Hello World!" << endl;
   return 0;

}

///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello3.cpp
/// @version 1.0
///
/// @author Rachel Watanabe <rkwatana@hawaii.edu>
/// @date 28_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
using namespace std;

class Cat {
public:
   void sayHello() {
      cout << "Meow!" << endl;
   }
} ;

int main() {

   Cat myCat;
   myCat.sayHello();

   return 0;
}
